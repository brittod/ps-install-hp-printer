﻿#No 32-bit support
$DNSName = $args #grab the variable from the batch file
$printerlist = import-csv \\team12server\printers\printerlist.csv|? DNSName -eq $DNSName #select the correct printer from the CSV file
$DriverShare = $printerlist.drivershare
$DriverINF = $printerlist.driverinf
$DriverName = $printerlist.drivername
$LastKnownIP = $printerlist.lastknownIP

$IP = try{([System.Net.Dns]::GetHostAddresses($DNSName))|? AddressFamily -eq "InterNetwork"|select -expandproperty IPAddressToString}catch{}

$ExistingIP = (gwmi win32_printer|? printerstate -eq 0|? name -eq $DNSName).portname
if ($IP.count -eq 0){$IP = $LastKnownIP}#if no IP found then use the static IP from the CSV file

if (($ExistingIP -ne $IP) -or ($existingIP.count -eq 0)){
#if existing printer found, delete the port, driver, and name so it can be updated with all new settings
if (($ExistingIP -ne $IP) -and ($existingIP.count -gt 0)){
#This will remove any spooled jobs then delete the printer with the old IP address
stop-service spooler
#removes any stuck print jobs
remove-item c:\windows\system32\spool\printers\*.* -force
start-service spooler
$settings = gwmi win32_printer|? name -eq $DNSName|select *
cscript c:\windows\system32\printing_admin_scripts\en-us\prnmngr.vbs -d -p $DNSName
cscript c:\windows\system32\printing_admin_scripts\en-us\prnport.vbs -d -r $settings.portname
#$d is a catcher of output from cscript prndrvr so it doesn't display at console
$d = cscript c:\windows\system32\printing_admin_scripts\en-us\prndrvr.vbs -x
}

write-host ("Printer: " + $DNSName)-f green
if ($IP.count -gt 0){write-host ("IP address: " + $IP)-f green}else{write-host ("No Active IP, using last known IP: " + $IP)-f red}
write-host ("$DNSName will be available once this window disappears (approx. 3 minutes).`n") -f white
$delprinters = cscript c:\windows\system32\printing_admin_scripts\en-us\prndrvr.vbs -x
cscript c:\windows\system32\printing_admin_scripts\en-us\prndrvr.vbs -a -m $DriverName -h $DriverShare -i ($DriverShare + "\" + $DriverINF)
cscript c:\windows\system32\printing_admin_scripts\en-us\prnport.vbs -a -r $IP -h $IP -o raw -n 9100
#two ways to remove a printer; either via rundll32 or cscript prnmngr
#rundll32 printui.dll,PrintUIEntry /if /b $DNSName /r $IP /m $DriverName
cscript c:\windows\system32\printing_admin_scripts\en-us\prnmngr.vbs -a -p $DNSName -m $DriverName -r $IP
}

if ($ExistingIP -eq $IP){write-host ($DNSName + " has already been setup for this location") -f green;timeout 20}