﻿<#
.Synopsis
   Function to install any HP brand networked printer
.DESCRIPTION
   Function to install any HP Brand networked printer using HP's Universal PCL5 Print Driver. Supply function with an IP address and what you would like to name the printer. 
   Name will appear as Windows see's it in future activites with the printer. 
.EXAMPLE
   e.g. C:\> Install-HPPrinter "Name you want for the printer" "IP Address of the printer"
.EXAMPLE
   e.g C:\> Install-HPPrinter "My Awesome Printer" "10.5.6.2"
#>


$ScriptPath = $MyInvocation.MyCommand.Path
$CurrentDir = Split-Path $ScriptPath

$INFPath = "$CurrentDir\PCL5\hpcu180t.inf"
$DriverName= 'HP Universal Printing PCL 5'

Function Install-HPPrinter {

  [CmdletBinding()]

Param(

    [Parameter(Mandatory = $true)]
    [String[]]$PrinterName,
    [String[]]$IP
    )

    Add-PrinterPort -Name "$IP" -PrinterHostAddress "$IP"
    PNPUtil.exe -I -A "$INFPath"
    Add-PrinterDriver -Name "$DriverName"
    Add-Printer -Name "$PrinterName" -DriverName "$DriverName" -PortName "$IP"
    
}

Function Install-LocalPrinter {

  $WMI = Get-WMIObject Win32_Printer -Property *
  $Printer = $WMI.CreateInstance()
  $Printer.Name = $Printer.DeviceID = 'Local Printer'
  $Printer.PortName = 'USB01'
  $Printer.Network = $false
  $Printer.Shared = $false
  $Printer.DriverName = $DriverName
  $Printer.Put()

}


<#

potential steps for local printer

$printerclass = [wmiclass]'Win32_Printer'
$printer = $printerclass.CreateInstance()
$printer.Name = $printer.DeviceID = 'NewPrinter'
$printer.PortName = 'LPT1:'
$printer.Network = $false
$printer.Shared = $true
$printer.ShareName = 'NewPrintServer'
$printer.Location = 'Office 12'
$printer.DriverName = 'HP LaserJet 3050 PCL5'
$printer.Put() 

#>